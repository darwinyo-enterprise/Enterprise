# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.0.0] - 2018-03-06
### Added
- New UI for Admin Modules to manage Product,Category, Manufacturers.
- New Functionality to store Images in server
- Complete Tests for Admin Modules Front end till backend

### Fixed

### Changed

## [1.1.0] - 2018-03-06
### Added

### Fixed
- fix e2e test
- fix frontend unit test
- fix CHANGELOG

### Changed
- Update Workspace to Nx@6
- Update Angular material @6.2.0

[Unreleased]: https://gitlab.com/darwinyo-enterprise/Enterprise/tree/dev/master
[1.0.0]: https://gitlab.com/darwinyo-enterprise/Enterprise/tags